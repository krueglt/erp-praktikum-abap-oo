# Chapter 10: Global Classes/Interfaces - Recap

When you have finished this exercise, you are able to:

- Create and edit global classes and interfaces, using Eclipse

Solution: [zcl\_##\_hotel, zif_##_partner, zcl_main_airplane_e13](solutions/Chapter%2010%20Global%20Classes%20-%20Recap) 

A travel agency communicates with its business partners. Those partners shall be the contracted hotels.

1. Create the global class `ZCL_##_HOTEL`. The class shall contain these attributes and methods:

    - `name` type `string` as a private instance attribute
    - `max_beds` type `i` as a private instance attribute
    - `constructor` importing values for `name` and `max_beds`.
    - `display_attributes` for displaying instance attributes

3. Implement the methods and activate your class.

4. Define the global interface `ZIF_##_PARTNER` containing the method `display_partner`.

5. Make `ZCL_##_HOTEL` implement this interface.

6. Switch back to your project and replace local interface `lif_partner` with global interface `ZIF_##_PARTNER`.

7. Create some hotels and add them to the business partners of your travel agency.
