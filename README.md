# Exercises ABAP Objects

In these exercises, you can continuously use your previous solution and add the next exercise to it.
If you need a clean state, you can also copy the solution of the previous exercise, and continue from that.

## Chapters

### [Chapter 3: Analysis and Design - UML](01_Design.md)

### [Chapter 4: Basics - Creating Classes](02_CreatingClasses.md)

### [Chapter 4: Basics - Instantiating Objects](03_InstantiatingObjects.md)

### [Chapter 4: Basics - Calling Methods](04_CallingMethods.md)

### [Chapter 4: Basics - Constructor](05_Constructor.md)

### [Chapter 4: Basics - Calling Private Methods](06_CallingPrivateMethods.md)

### [Chapter 5: Inheritance - Creating Class Hierarchies](07_Inheritance.md)

### [Chapter 6: Casting - Polymorphy](08_Polymorphy.md)

### [Chapter 7: Exceptions - Defining, Triggering, Propagating and Catching](09_Exceptions.md)

### [Chapter 8: Interfaces - Defining Interfaces](10_DefiningInterfaces.md)

### [Chapter 8: Interfaces - Implementing Interfaces](11_ImplementingInterfaces.md)

### [Chapter 9: Events - Raising and Handling Events](12_Events.md)

### [Chapter 10: Global Classes/Interfaces - Recap](13_GlobalClasses.md)

### [Chapter 11: Advanced Techniques - Singleton Classes](14_Singleton.md)

### [(Optional) Chapter 12:  Advanced Techniques - Friend Relationships](15_Friends.md)
