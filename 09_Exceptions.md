# Chapter 7: Exceptions - Defining, Triggering, Propagating and Catching

When you have finished this exercise, you are able to:

- Define exception classes
- Raise class based exceptions
- Propagate class based exceptions
- Catch and handle class based exceptions

Solution: [zcl_main_airplane_e9, zcx_unknown_planetype](solutions/Chapter%207%20Exceptions%20-%20Defining,%20Triggering,%20Propagating%20and%20Catching)

1. Define a global exception class `ZCX_##_UNKNOWN_PLANETYPE`.
   Choose the super class so that handling or propagating is mandatory by syntax.

2. Raise the exception from the method `get_technical_attributes` of your local class `lcl_airplane`,
   when technical attributes cannot be determined for a given plane type.
   Add the `RAISING` clause to the method definition, so the exception may be propagated.

3. Test your program, which is up to crash in its current state with an ABAP runtime error.
   Look at the details of that error and try to find out where the error came from.

4. Catch the exception at a suitable position within your program.
   Find that position by considering that as much as possible of the intended output shall appear on the screen.
   You need a local reference variable to the exception object (e.g. `lx_unknown_planetype`). Choose a suitable type for it on your own.
   Put the error message from the exception object into your output by using the method `get_text`. What message does it show?

OPTIONAL:

5. Add a public attribute `planetype` of type `/dmo/plane_type_id` to your exception class.
   Extend the constructor with a parameter to set the class attribute.
   In the catch clause, access the public attribute and output it in addition to the error message.

6. Add custom text to the exception:

    - create a message class, or use existing an existing one you already created
    - add a message to the message class: “Unknown plane type.”
    - use the code template `textIdExceptionClass` to add a constant for that message to the exception class (public area).
    - change the constructor implementation so that it uses this message as default message.
