# Chapter 4: Basics - Instantiating Objects

When you have finished this exercise, you are able to:

- Instantiate objects.

Solution: [zcl_main_airplane_e3](solutions/Chapter%204%20Basics%20-%20Instantiating%20Objects)

1. In the main method: Define a reference variable, which points to objects of class `lcl_airplane`.

2. Define an internal table for buffering objects of class `lcl_airplane`.
The table should have the line type `REF TO lcl_airplane`.

3. Create four objects of `lcl_airplane` and save them within the internal table you just created.

4. Set a breakpoint at the start of the main method and use the debugger to observe the process of your program.
Examine the state of the process by inspecting variables.
