# Chapter 5: Inheritance - Creating Class Hierarchies

When you have finished this exercise, you are able to:

- Define sub classes
- Redefine methods from super classes in sub classes

Solution: [zcl_main_airplane_e7](solutions/Chapter%205%20Inheritance%20-%20Creating%20Class%20Hierarchies)

1. Make the two instance attributes in the class `lcl_airplane` visible for its sub classes (`PRIVATE SECTION` → `PROTECTED SECTION`).

2. Create the sub class `lcl_passenger_plane` for the class `lcl_airplane`.

    - The class shall contain a private instance attribute `max_seats` typed with `i` (integer).
    - Define and implement a public constructor within the class, setting all the instance attributes.
    - Redefine the method `display_attributes` from `lcl_airplane` in `lcl_passenger_plane`, so it displays all attributes.

3. Create the sub class `lcl_cargo_plane` for the class `lcl_airplane`.

    - The class shall contain a private instance attribute `max_cargo` typed with `i` (integer).
    - Define and implement a public constructor within the class, setting all the instance attributes.
    - Redefine the method `display_attributes` from `lcl_airplane` in `lcl_cargo_plane`, so it displays all attributes.

4. Switch to your main program.

    - Create a reference variable for the sub classes `lcl_passenger_plane` and `lcl_cargo_plane` using the `DATA` statement.
    - Call the static method `display_number_of_airplanes` before you instantiate an object.
    - Create objects for each of the sub classes `lcl_passenger_plane` and `lcl_cargo_plane`. Choose arbitrary values for the attributes.
    - Call the method `display_attributes` for each of the objects.
    - Call the method `display_number_of_airplanes` again.

6. Observe the process of the program using the debugger, especially the calls of the method `display_attributes`.

7. Is it possible to call the method `get_technical_attributes` directly from the redefined method `display_attributes` within the sub classes?

8. If the attributes `name` and `planetype` would be kept private, how could the sub classes access these attributes?
