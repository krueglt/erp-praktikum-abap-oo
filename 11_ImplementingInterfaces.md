# Chapter 8: Interfaces - Implementing Interfaces

When you have finished this exercise, you are able to:

- Define and implement interfaces
- Use polymorphy regarding interfaces

Solution: [zcl_main_airplane_e11, uml](solutions/Chapter%208%20Interfaces%20-%20Implementing%20Interfaces)

1. The class `lcl_travel_agency` shall be added to the current UML diagram (see below). 
   It shall be able to access the classes `lcl_rental`, `lcl_carrier` and `lcl_hotel` using the interface `lif_partner`.

2. Create the local class `lcl_travel_agency` in the following way:

    - The travel agency shall add business partners (airlines, rentals, hotels) to its internal table `partners` by using the method `add_partner`.
    - Which type do the internal table and the parameter of `add_partner` have?
    - Using the method `display_agency_partners`, an agency shall be able to display its partners’ data.

4. Switch to your main program.

    - Create an object of `lcl_travel_agency`.
    - Add the travel agency’s partners by using the method `add_partner`.
    - Display the agency’s partners using the method `display_agency_partners`.
    - Where does the polymorphy take place here?
    - Try tracking the program with the debugger.

![img.png](11_UML.png)
