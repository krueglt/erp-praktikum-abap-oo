# (Optional) Chapter 12:  Advanced Techniques - Friend Relationships

When you have finished this exercise, you are able to:

- Create a friend relationship between classes and exchange data between the related classes.

Note: The `friend` concept is mostly only used in large projects with a complex class structure
which is thoroughly planned and where other solutions have been deeply discussed.

Solution: Coming soon

1. Use the singleton and the main program you used in the previous exercise.

    - make the attribute `shared_text` in the singleton class private

2. Create a class ZCL_##_FRIEND that can access the private attribute using a friend relationship.

    - the class shall have a public method `get_shared_text`, returning a string
    - implement the method so that it gets an instance of the singleton class,
      reads its `shared_text` attribute, and returns that value.

3. Switch to your singleton class, add `ZCL_##_FRIEND` as a friend of your singleton.

4. In the singleton main class `ZCL_##_SINGLETON_MAIN`:

    - Check what happens when you try to execute the previous code.
    - Create an object of type `ZCL_##_FRIEND` within your main program.
    - Replace the direct access to `shared_text` with calls to the method `get_shared_text`
