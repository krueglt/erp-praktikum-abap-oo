# Chapter 8: Interfaces - Defining Interfaces

When you have finished this exercise, you are able to:

- Define and implement interfaces

Solution: [zcl_main_airplane_e10, uml](solutions/Chapter%208%20Interfaces%20-%20Defining%20Interfaces)

The classes `lcl_rental` and `lcl_hotel` were added to the UML diagram below.
During the exercise the classes `lcl_rental`, `lcl_carrier` and later `lcl_hotel` shall outsource common services
and provide an interface for users, which will be called `lif_partner`.

1. Add the missing classes and the interface `lif_partner` to your UML diagram.
   The only method that shall be provided in the interface is `display_partner`.
   Think about what role `lcl_carrier`, `lcl_rental` and `lcl_hotel` have to fulfill.

2. Copy the interface `lif_partner` and the class `lcl_rental` to the local types of your class.
   [Templates to copy](10_templates.txt)

4. Make class `lcl_carrier` implement interface `lif_partner`.

5. Create some objects of `lcl_rental` and `lcl_carrier` in your main program.

(Usage of the interface is part of the next exercise.)

![img.png](10_UML.png)
