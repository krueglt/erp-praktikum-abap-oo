# Chapter 11: Advanced Techniques - Singleton Classes

When you have finished this exercised, you are able to:

- Create a singleton

Solution: [zcl\_##\_singleton, zcl_##_singleton_main](solutions/Chapter%2011%20Advanced%20Techniques%20-%20Singleton%20Classes)

1. Create the global class `ZCL_##_SINGLETON`. The class should meet the following requirements:

    - Creating objects shall be only allowed from within the class itself.
    - The class shall be only instantiated exactly once.
    - The class shall contain a static reference `instance` to the object created.
      The very first access to the class shall trigger creating the only one object.
    - The class shall have a public attribute `shared_text` of type `string`.
      When the object is created, the value shall be set to `'initial value'`
    - The class shall contain a method `get_instance`, returning the object reference.
    - What is the reason the method `get_instance` has to be static?

2. Create a new global class `ZCL_##_SINGLETON_MAIN` with a main method (implementing interface `if_oo_adt_classrun`).

3. Verify your singleton design in the main method:

    - Declare two variables, both type `REF TO ZCL_##_SINGLETON`.
    - Use `get_instance` of `ZCL_##_SINGLETON` to get an instance of that class, for both variables.
    - Use `out->write` to check the value of the attribute `shared_text` for both instances
    - set the public attribute `shared_text` to a new value, using only one of the variables.
    - Use `out->write` to check the value of the attribute `shared_text` for both instances
