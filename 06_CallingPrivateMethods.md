# Chapter 4: Basics - Calling Private Methods

When you have finished this exercise, you are able to:

- Call a method within a class

Solution: [zcl_main_airplane_e6, data generator class](solutions/Chapter%204%20Basics%20-%20Calling%20Private%20Methods);
The DDIC objects are not shown in the solution.

###	Recap: Create DDIC objects and data

1. Create DDIC objects:

    - Domain `ZRAPH_##_AIRPLANE_WEIGHT`, type `int4`
    - Data element `ZRAPH_##_AIRPLANE_WEIGHT`, based on that domain
    - Database table `ZRAPH_##_AIRPLN` with fields:

        - `client`, type `abap.clnt`
        - `type_id` type `/dmo/plane_type_id`
        - `weight` type `ZRAPH_##_AIRPLANE_WEIGHT`

2. Optional: Create a data generator class, and fill the table with some values.
You can check database table `/DMO/FLIGHT` for values used for plane type ids (or make up your own)

### Private method calls

1. Create the private instance method `get_technical_attributes` within class `lcl_airplane`.

    - This method shall import a value by a parameter typed with the plane type id data element.
    - The method shall export the planes weight in parameter ex_weight.
    - The export parameter shall be filled with the value read from the database table created in the step above, queried on the given plane type.
    - If there is no record in the table for the given plane type, then the default value weight: 100.000 shall be returned.
      If you skipped the optional step 2 above, the select will never find data, and the default value will always be returned.
      Otherwise you should see the values you have stored in the database table for a corresponding planetype.
    - Test your method `get_technical_attributes`

        - by calling it from the main program;
        - by calling it within the method display_attributes.
        - Which of the two ways was permitted/successful and why?
