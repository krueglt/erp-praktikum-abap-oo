# Chapter 4: Basics - Creating Classes

When you have finished this exercise, you are able to:

- Create an ABAP local class

Solution: [zcl_main_airplane_e2](solutions/Chapter%204%20Basics%20-%20Creating%20Classes)

### General note

We are using `out->write()` to display data on the console.
`out` is only available in the main method, it is a parameter of the main method, as declared in the interface `if_oo_adt_classrun`.
Each method that needs to write data to the console needs access to this `out` reference, and therefore needs an importing parameter for it.
This applies to all exercises.

Example:

     METHODS display_attributes
        IMPORTING im_out TYPE REF TO if_oo_adt_classrun_out.

### Exercise

1. Create the main class `ZCL_##_MAIN_AIRPLANE` (##: user number/initials)

2. Create the class `lcl_airplane` in the Local Types of that class

    1. The class shall contain two private attributes:
        - `name`
        - `planetype`
    
        The attribute `name` shall be of type `string`.
        The attribute `planetype` shall be of the same type as the table field `/DMO/FLIGHT-plane_type_id`.

    2. The class shall contain one private, static attribute:

        - `number_of_airplanes`

        This attribute shall be of type `i` (integer).

    4. The class shall contain a public instance method `set_attributes` used for assigning values to the private attributes
       `name` and `planetype`. Declare the method in the definition part to accept two importing parameters that match those attributes.
       Implement the method in the implementation part of the class.
       Make it set the two attributes.
       Upon every call of this method, the static member `number_of_airplanes` shall be incremented by one.

    5. The class shall contain another public instance method `display_attributes` for displaying the instance attributes.
       Declare the method and add an empty implementation.
       Add an importing parameter for writing data to the console, as explained in the general note above.

    6. Declare the public static method `display_number_of_airplanes` (for displaying the attribute
       `number_of_airplanes`), and add an empty implementation.
       This method will also need the importing parameter for writing data to the console.
